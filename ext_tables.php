<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hny_ttcontent_loop', 'Configuration/TypoScript', 'Loop');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hnyttcontentloop_domain_model_element', 'EXT:hny_ttcontent_loop/Resources/Private/Language/locallang_csh_tx_hnyttcontentloop_domain_model_element.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hnyttcontentloop_domain_model_element');

    }
);
